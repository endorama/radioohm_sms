$ ->
  if ($('section.flash').children().length != 0)
    $('section.flash').fadeIn();

class window.Flash
  $flash_identifier: $('section.flash')

  add_flash: (message, type = 'alert') ->
    alert_button = '<button type="button" class="close" data-dismiss="alert">×</button>';

    if type != 'alert'
      css_class = ' alert-' + type
    else
      css_class = ''

    @$flash_identifier.append('<div class="alert' + css_class + '"><button type="button" class="close" data-dismiss="alert">×</button>' + message + '</div>');

  display: ->
    # @$flash_identifier.fadeIn()

  hide: ->
    @$flash_identifier.fadeOut()
    @$flash_identifier.html('')

    
  add_alert: (message) ->
    @add_flash(message)
  
  add_error: (message) ->
    @add_flash(message, 'error')

  add_info: (message) ->
    @add_flash(message, 'info')

  add_success: (message) ->
    @add_flash(message, 'success')
  
  display_alert: (message) ->
    @add_alert(message)
    @display()

  display_error: (message) ->
    @add_error(message)
    @display()

  display_info: (message) ->
    @add_info(message)
    @display()

  display_success: (message) ->
    @add_success(message)
    @display()

