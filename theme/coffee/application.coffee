#= require sms
#= require twitter
#= require contacts
#= require api
#= require flash

$ ->
  api_handler = new Api()
  flash_handler = new Flash()

  # enable tooltips
  $('[data-toggle="tooltip"]').tooltip
    placement: 'bottom'

  # enable popovers
  $('[data-toggle="popover"]').popover
    title: '<i class="icon-info-sign"></i> Informazioni',
    trigger: 'hover'

  # bind link to scroll to top action
  $('[data-action="to-top"]').click (event) ->
    event.preventDefault()

    $('html, body').animate({ scrollTop: 0 }, 'slow');

  # fetch all content for the first time
  api_handler.fetch_all()

  ## SMS

  # start countdown, to show user next fetch
  $('#countdown').countdown
    start: 15,
    callBack: ->
      api_handler.fetch_new()
      $('#countdown').countdown('reset')

  # $.doTimeout 'new_messages', 10000, ->
  #   api_handler.fetch_new()
  #   $('#countdown').countdown('reset')
  #   $('#countdown').countdown('start')
  
  # load new sms on click
  $('[data-action="load-new-sms"]').click (event) ->
    event.preventDefault()

    api_handler.fetch_new()
    $('#countdown').countdown('reset')
    $('#countdown').countdown('start')

  # enable automatic loading of new sms
  $('[data-action="enable-automatic-sms-refresh"]').click (event) ->
    event.preventDefault()

    $('i.icon-ok', $(this).parent().parent()).remove()
    
    $('#countdown').countdown('reset')
    $('#countdown').countdown('start')

    $(this).append('<i class="icon-ok"></i>')

  # disable automatic loading of new sms
  $('[data-action="disable-automatic-sms-refresh"]').click (event) ->
    event.preventDefault()

    $('i.icon-ok', $(this).parent().parent()).remove()
    

    $('#countdown').countdown('stop')
    $('#countdown').text('mai')

    $(this).append('<i class="icon-ok"></i>')

  ## end SMS

  ## Set `read` onclick
  $('table tr').live 'click', (e) ->
    # console.log('click')
    # console.log(this)
    # console.log($(this).attr('data-type') + ' ' + $(this).attr('data-index'))

    if ($(this).attr('data-type') == 'sms')
      api_handler.sms.read($(this).attr('data-index'))
    else if ($(this).attr('data-type') == 'twitter')
      api_handler.twitter.read($(this).attr('data-index'))

  ## Set `unread` on duoble click
  # $('table tr').live 'dblclick', (e) ->
  #   console.log('doubleclick')
  #   # console.log(this)
  #   console.log($(this).attr('data-type') + ' ' + $(this).attr('data-index'))

  $('[data-action="read-all"]').click (event) ->
    event.preventDefault()
    $('table tr').each (index, value) ->
      if $(value).attr('data-read') == '0'
        if $(value).attr('data-type') == 'sms'
          api_handler.sms.read($(value).attr('data-index'))
        else if $(value).attr('data-type') == 'twitter'
          api_handler.twitter.read($(value).attr('data-index'))
