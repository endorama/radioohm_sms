class window.Api
  api_baseurl: '/radioohm_sms/api/'
  read_time  : 10000
  
  constructor: (@options = {}) ->
    @sms = new Sms
      'url': @api_baseurl
      'read_time': @read_time
    @twitter = new Twitter
      'url': @api_baseurl
      'read_time': @read_time

  api_url: (url) =>
    @api_baseurl + url

  display: () ->
    if not @messages
      return

    html = ''
    for re in @messages
      html += '
<tr id="' + re.type + '-' + re.index + '" data-index="' + re.index + '" data-type="' + re.type + '" data-read="' + re.letto + '" data-timestamp="' + re.timestamp + '">
  <td><i class="source-' + re.type + '"></i></td>
  <td>' + re.mittente + '</td>
  <td>' + re.messaggio + '</td>
  <td>' + re.ora + '</td>
</tr>'
    
    if $('table tbody').children().length == 0
      $('table tbody').html(html)
    else
      Tinycon.setBubble(@messages.length);
      $('table tbody').prepend(html)

    @messages = null

  fetch_all: ->
    @sms.fetch()
    @twitter.fetch()

    $.doTimeout 1000, =>
      @_sort_results()
      @display()

  fetch_new: ->
    @sms.fetch_new()
    @twitter.fetch_new()

    $.doTimeout 1000, =>
      @_sort_results()
      @display()

  _sort_results: () ->
    if @sms.response && @twitter.response
      @messages = @sms.response.concat @twitter.response
      @messages.sort (a, b) ->
        return b['timestamp'] - a['timestamp']
    else if @sms.response && !@twitter.response
      @messages = @sms.response
    else if @twitter.response && !@sms.response
      @messages = @twitter.response

    @sms.response = null
    @twitter.response = null
