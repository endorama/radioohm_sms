class window.Sms
  
  constructor: (@options = {}) ->   
    @url = @options.url + 'sms'
    @read_time = @options.read_time

  # display: () ->
  #   @_display_fetched_data(@response)
  #   @response = null

  fetch: (id) =>
    if (id)
      url =  @url + "/#{id}"
    else
      url = @url

    $.ajax
      url: url,
      type: 'GET',
      error: (response) ->
        alert(response.responseText)
      success: (response) =>
        @response = @_add_type(response)
        Flash.prototype.display_info('Fetched messages.')

  fetch_new: () =>
    $.ajax
      url: @url + "/new",
      type: 'GET',
      data: { 'index': $($('[data-type="sms"]')[0]).attr('data-index') },
      error: (response) ->
        alert(response.responseText)
      success: (response) =>
        # console.log(response)

        if (response != 'null')
          @response = @_add_type(response)
          Flash.prototype.display_info('New messages.')
        else
          Flash.prototype.hide()
          Flash.prototype.display_info('There are no new messages.')
          
        $.doTimeout @read_time, ->
          Flash.prototype.hide()

  read: (id) =>
    $.ajax
      url: @url + "/#{id}",
      type: 'PUT',
      data:
        letto: 1
      error: (response) ->
        alert(response.responseText)
      success: (response) ->
        response = response[0]
        $('#sms-' + id).addClass('read').data('read', response.letto)

  _add_type: (response) ->
    for re in response
      re.type = 'sms'

    response

#   _display_fetched_data: (data) ->
#     html = ''

#     for re in data
#       html += '
# <tr id="sms-' + re.index + '" data-read="' + re.letto + '" data-time="' + re.timestamp + '">
#   <td><i class="source-sms"></i></td>
#   <td>' + re.mittente + '</td>
#   <td>' + re.messaggio + '</td>
#   <td>' + re.ora + '</td>
# </tr>'
    
#     if $('table tbody').children().length == 0
#       $('table tbody').html(html)
#     else
#       $('table tbody').prepend(html)

#     $.doTimeout @read_time, () ->
#       Flash.prototype.hide()

#       for re in data
#         if re.letto != '1'
#           @read(re.index)
