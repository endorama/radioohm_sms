class window.Contacts
  
  constructor: (@options = {}) ->   
    @url = @options.url + 'contacts'
    @_response = null
  
  fetch: (id) ->
    if (id)
      url = @url + "/#{id}"
    else
      url = @url
    
    $.ajax
      url: url,
      type: 'GET',
      error: (response) ->
        alert(response.responseText)
      success: (response) =>
        @_response = response
        Flash.prototype.display_info('Fetched contacts.')

  display: ->
    # console.log(@)
    # console.log(@_response)
    if (not @_response)
      $.doTimeout 100, =>
        @display()
    else
      for re in @_response
        $('table tbody td:contains(' + re.phone_number + ')').text(re.display_name)
