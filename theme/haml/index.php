<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class='no-js' lang='en'>
  <!--<![endif]-->
  <head>
    <meta charset='utf-8'>
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
    <title></title>
    <meta content='' name='description'>
    <meta content='' name='author'>
    <meta content='width=device-width, initial-scale=1.0' name='viewport'>
    <link href='css/normalize.css' rel='stylesheet'>
    <link href='css/screen.css' rel='stylesheet'>
    <script src='js/vendor/modernizr-2.6.1.min.js'></script>
  </head>
  <body>
    <div class='<?php echo $asd ?>' id='container'>
      test
      <header id='header'>
        <div class='navbar navbar-fixed-top navbar-inverse'>
          <div class='navbar-inner'>
            <a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </a>
            <a class='brand' href='#'>
              RadioOhm.it
            </a>
            <div class='nav-collapse'>
              <ul class='nav'>
                <li>
                  <a data-action='to-top' data-toggle='tooltip' href='#' title='Torna a inizio pagina'>
                    <i class='icon-circle-arrow-up icon-white'></i>
                  </a>
                </li>
                <li>
                  <a data-action='load-new-sms' data-toggle='tooltip' href='#' title='Verifica la presenza di nuovi sms'>
                    <i class='icon-refresh icon-white'></i>
                  </a>
                </li>
                <li class='dropdown'>
                  <a class='dropdown-toggle' data-action='automatic-sms-refresh' data-toggle='dropdown' href='#'>
                    Aggiornameto automatico
                  </a>
                  <ul aria-labelledby='dLabel' class='dropdown-menu' role='menu'>
                    <li>
                      <a data-action='enable-automatic-sms-refresh' data-content='Se abilitato, ogni 30 secondi il programma caricherà i nuovi sms, se ve ne sono' data-toggle='popover' href='#' title=''>
                        Abilitato
                        <i class='icon-ok'></i>
                      </a>
                    </li>
                    <li>
                      <a data-action='disable-automatic-sms-refresh' data-content='Il programma non cercherà nuovi sms. Puoi aggiornare manualmente con il pulsante nella barra superiore!' data-toggle='popover' href='#' title=''>
                        Disabilitato
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <p class='navbar-text countdown-container'>
              Prossimo aggiornamento automatico
              <span id='countdown'>mai</span>
            </p>
          </div>
        </div>
      </header>
      <div id='main' role='main'>
        <section class='flash'></section>
        <table class='table table-striped table-condensed table-bordered table-hover'>
          <thead>
            <th>Origine</th>
            <th>Mittente</th>
            <th>Messaggio</th>
            <th>Ora</th>
          </thead>
          <tbody></tbody>
          <tfoot>
            <th>Origine</th>
            <th>Mittente</th>
            <th>Messaggio</th>
            <th>Ora</th>
          </tfoot>
        </table>
      </div>
      <footer id='footer'></footer>
    </div>
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'></script>
    <script>
      window.jQuery || document.write("<script src='js/vendor/jquery-1.8.0.min.js'>\x3C/script>")
    </script>
    <script src='js/vendor/bootstrap.js'></script>
    <script src='js/plugins.js'></script>
    <script src='../jquery.jsonp.js'></script>
    <script src='../jquery-twitter-plugin.js'></script>
    <script src='js/application.js'></script>
    <!--[if lt IE 7]>
    <script src='//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js'></script>
    <script>
      window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})});
    </script>
    <![endif]-->
  </body>
</html>
