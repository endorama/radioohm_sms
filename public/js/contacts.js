/* Compiled by Sprockets! using therubyracer (V8) */
(function() {

  window.Contacts = (function() {

    function Contacts(options) {
      this.options = options != null ? options : {};
      this.url = this.options.url + 'contacts';
      this._response = null;
    }

    Contacts.prototype.fetch = function(id) {
      var url,
        _this = this;
      if (id) {
        url = this.url + ("/" + id);
      } else {
        url = this.url;
      }
      return $.ajax({
        url: url,
        type: 'GET',
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          _this._response = response;
          return Flash.prototype.display_info('Fetched contacts.');
        }
      });
    };

    Contacts.prototype.display = function() {
      var re, _i, _len, _ref, _results,
        _this = this;
      if (!this._response) {
        return $.doTimeout(100, function() {
          return _this.display();
        });
      } else {
        _ref = this._response;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          re = _ref[_i];
          _results.push($('table tbody td:contains(' + re.phone_number + ')').text(re.display_name));
        }
        return _results;
      }
    };

    return Contacts;

  })();

}).call(this);
