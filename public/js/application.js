/* Compiled by Sprockets! using therubyracer (V8) */
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Sms = (function() {

    function Sms(options) {
      this.options = options != null ? options : {};
      this.read = __bind(this.read, this);

      this.fetch_new = __bind(this.fetch_new, this);

      this.fetch = __bind(this.fetch, this);

      this.url = this.options.url + 'sms';
      this.read_time = this.options.read_time;
    }

    Sms.prototype.fetch = function(id) {
      var url,
        _this = this;
      if (id) {
        url = this.url + ("/" + id);
      } else {
        url = this.url;
      }
      return $.ajax({
        url: url,
        type: 'GET',
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          _this.response = _this._add_type(response);
          return Flash.prototype.display_info('Fetched messages.');
        }
      });
    };

    Sms.prototype.fetch_new = function() {
      var _this = this;
      return $.ajax({
        url: this.url + "/new",
        type: 'GET',
        data: {
          'index': $($('[data-type="sms"]')[0]).attr('data-index')
        },
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          if (response !== 'null') {
            _this.response = _this._add_type(response);
            Flash.prototype.display_info('New messages.');
          } else {
            Flash.prototype.hide();
            Flash.prototype.display_info('There are no new messages.');
          }
          return $.doTimeout(_this.read_time, function() {
            return Flash.prototype.hide();
          });
        }
      });
    };

    Sms.prototype.read = function(id) {
      return $.ajax({
        url: this.url + ("/" + id),
        type: 'PUT',
        data: {
          letto: 1
        },
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          response = response[0];
          return $('#sms-' + id).addClass('read').data('read', response.letto);
        }
      });
    };

    Sms.prototype._add_type = function(response) {
      var re, _i, _len;
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        re = response[_i];
        re.type = 'sms';
      }
      return response;
    };

    return Sms;

  })();

}).call(this);
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Twitter = (function() {

    function Twitter(options) {
      this.options = options != null ? options : {};
      this.read = __bind(this.read, this);

      this.fetch_new = __bind(this.fetch_new, this);

      this.fetch = __bind(this.fetch, this);

      this.url = this.options.url + 'twitter';
      this.read_time = this.options.read_time;
    }

    Twitter.prototype.fetch = function(id) {
      var url,
        _this = this;
      if (id) {
        url = this.url + ("/" + id);
      } else {
        url = this.url;
      }
      return $.ajax({
        url: url,
        type: 'GET',
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          _this.response = _this._add_type(response);
          return Flash.prototype.display_info('Fetched twitter messages.');
        }
      });
    };

    Twitter.prototype.fetch_new = function() {
      var _this = this;
      return $.ajax({
        url: this.url + "/new",
        type: 'GET',
        data: {
          'index': $($('[data-type="twitter"]')[0]).attr('data-index')
        },
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          if (response !== 'null') {
            _this.response = _this._add_type(response);
            Flash.prototype.display_info('New messages.');
          } else {
            Flash.prototype.hide();
            Flash.prototype.display_info('There are no new messages.');
          }
          return $.doTimeout(_this.read_time, function() {
            return Flash.prototype.hide();
          });
        }
      });
    };

    Twitter.prototype.read = function(id) {
      return $.ajax({
        url: this.url + ("/" + id),
        type: 'PUT',
        data: {
          letto: 1
        },
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          response = response[0];
          return $('#twitter-' + id).addClass('read').data('read', response.letto);
        }
      });
    };

    Twitter.prototype._add_type = function(response) {
      var re, _i, _len;
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        re = response[_i];
        re.type = 'twitter';
      }
      return response;
    };

    return Twitter;

  })();

}).call(this);
(function() {

  window.Contacts = (function() {

    function Contacts(options) {
      this.options = options != null ? options : {};
      this.url = this.options.url + 'contacts';
      this._response = null;
    }

    Contacts.prototype.fetch = function(id) {
      var url,
        _this = this;
      if (id) {
        url = this.url + ("/" + id);
      } else {
        url = this.url;
      }
      return $.ajax({
        url: url,
        type: 'GET',
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          _this._response = response;
          return Flash.prototype.display_info('Fetched contacts.');
        }
      });
    };

    Contacts.prototype.display = function() {
      var re, _i, _len, _ref, _results,
        _this = this;
      if (!this._response) {
        return $.doTimeout(100, function() {
          return _this.display();
        });
      } else {
        _ref = this._response;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          re = _ref[_i];
          _results.push($('table tbody td:contains(' + re.phone_number + ')').text(re.display_name));
        }
        return _results;
      }
    };

    return Contacts;

  })();

}).call(this);
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Api = (function() {

    Api.prototype.api_baseurl = '/radioohm_sms/api/';

    Api.prototype.read_time = 10000;

    function Api(options) {
      this.options = options != null ? options : {};
      this.api_url = __bind(this.api_url, this);

      this.sms = new Sms({
        'url': this.api_baseurl,
        'read_time': this.read_time
      });
      this.twitter = new Twitter({
        'url': this.api_baseurl,
        'read_time': this.read_time
      });
    }

    Api.prototype.api_url = function(url) {
      return this.api_baseurl + url;
    };

    Api.prototype.display = function() {
      var html, re, _i, _len, _ref;
      if (!this.messages) {
        return;
      }
      html = '';
      _ref = this.messages;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        re = _ref[_i];
        html += '\
<tr id="' + re.type + '-' + re.index + '" data-index="' + re.index + '" data-type="' + re.type + '" data-read="' + re.letto + '" data-timestamp="' + re.timestamp + '">\
  <td><i class="source-' + re.type + '"></i></td>\
  <td>' + re.mittente + '</td>\
  <td>' + re.messaggio + '</td>\
  <td>' + re.ora + '</td>\
</tr>';
      }
      if ($('table tbody').children().length === 0) {
        $('table tbody').html(html);
      } else {
        Tinycon.setBubble(this.messages.length);
        $('table tbody').prepend(html);
      }
      return this.messages = null;
    };

    Api.prototype.fetch_all = function() {
      var _this = this;
      this.sms.fetch();
      this.twitter.fetch();
      return $.doTimeout(1000, function() {
        _this._sort_results();
        return _this.display();
      });
    };

    Api.prototype.fetch_new = function() {
      var _this = this;
      this.sms.fetch_new();
      this.twitter.fetch_new();
      return $.doTimeout(1000, function() {
        _this._sort_results();
        return _this.display();
      });
    };

    Api.prototype._sort_results = function() {
      if (this.sms.response && this.twitter.response) {
        this.messages = this.sms.response.concat(this.twitter.response);
        this.messages.sort(function(a, b) {
          return b['timestamp'] - a['timestamp'];
        });
      } else if (this.sms.response && !this.twitter.response) {
        this.messages = this.sms.response;
      } else if (this.twitter.response && !this.sms.response) {
        this.messages = this.twitter.response;
      }
      this.sms.response = null;
      return this.twitter.response = null;
    };

    return Api;

  })();

}).call(this);
(function() {

  $(function() {
    if ($('section.flash').children().length !== 0) {
      return $('section.flash').fadeIn();
    }
  });

  window.Flash = (function() {

    function Flash() {}

    Flash.prototype.$flash_identifier = $('section.flash');

    Flash.prototype.add_flash = function(message, type) {
      var alert_button, css_class;
      if (type == null) {
        type = 'alert';
      }
      alert_button = '<button type="button" class="close" data-dismiss="alert">×</button>';
      if (type !== 'alert') {
        css_class = ' alert-' + type;
      } else {
        css_class = '';
      }
      return this.$flash_identifier.append('<div class="alert' + css_class + '"><button type="button" class="close" data-dismiss="alert">×</button>' + message + '</div>');
    };

    Flash.prototype.display = function() {};

    Flash.prototype.hide = function() {
      this.$flash_identifier.fadeOut();
      return this.$flash_identifier.html('');
    };

    Flash.prototype.add_alert = function(message) {
      return this.add_flash(message);
    };

    Flash.prototype.add_error = function(message) {
      return this.add_flash(message, 'error');
    };

    Flash.prototype.add_info = function(message) {
      return this.add_flash(message, 'info');
    };

    Flash.prototype.add_success = function(message) {
      return this.add_flash(message, 'success');
    };

    Flash.prototype.display_alert = function(message) {
      this.add_alert(message);
      return this.display();
    };

    Flash.prototype.display_error = function(message) {
      this.add_error(message);
      return this.display();
    };

    Flash.prototype.display_info = function(message) {
      this.add_info(message);
      return this.display();
    };

    Flash.prototype.display_success = function(message) {
      this.add_success(message);
      return this.display();
    };

    return Flash;

  })();

}).call(this);
(function() {

  $(function() {
    var api_handler, flash_handler;
    api_handler = new Api();
    flash_handler = new Flash();
    $('[data-toggle="tooltip"]').tooltip({
      placement: 'bottom'
    });
    $('[data-toggle="popover"]').popover({
      title: '<i class="icon-info-sign"></i> Informazioni',
      trigger: 'hover'
    });
    $('[data-action="to-top"]').click(function(event) {
      event.preventDefault();
      return $('html, body').animate({
        scrollTop: 0
      }, 'slow');
    });
    api_handler.fetch_all();
    $('#countdown').countdown({
      start: 15,
      callBack: function() {
        api_handler.fetch_new();
        return $('#countdown').countdown('reset');
      }
    });
    $('[data-action="load-new-sms"]').click(function(event) {
      event.preventDefault();
      api_handler.fetch_new();
      $('#countdown').countdown('reset');
      return $('#countdown').countdown('start');
    });
    $('[data-action="enable-automatic-sms-refresh"]').click(function(event) {
      event.preventDefault();
      $('i.icon-ok', $(this).parent().parent()).remove();
      $('#countdown').countdown('reset');
      $('#countdown').countdown('start');
      return $(this).append('<i class="icon-ok"></i>');
    });
    $('[data-action="disable-automatic-sms-refresh"]').click(function(event) {
      event.preventDefault();
      $('i.icon-ok', $(this).parent().parent()).remove();
      $('#countdown').countdown('stop');
      $('#countdown').text('mai');
      return $(this).append('<i class="icon-ok"></i>');
    });
    $('table tr').live('click', function(e) {
      if ($(this).attr('data-type') === 'sms') {
        return api_handler.sms.read($(this).attr('data-index'));
      } else if ($(this).attr('data-type') === 'twitter') {
        return api_handler.twitter.read($(this).attr('data-index'));
      }
    });
    return $('[data-action="read-all"]').click(function(event) {
      event.preventDefault();
      return $('table tr').each(function(index, value) {
        if ($(value).attr('data-read') === '0') {
          if ($(value).attr('data-type') === 'sms') {
            return api_handler.sms.read($(value).attr('data-index'));
          } else if ($(value).attr('data-type') === 'twitter') {
            return api_handler.twitter.read($(value).attr('data-index'));
          }
        }
      });
    });
  });

}).call(this);
