/* Compiled by Sprockets! using therubyracer (V8) */
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Api = (function() {

    Api.prototype.api_baseurl = '/radioohm_sms/api/';

    Api.prototype.read_time = 10000;

    function Api(options) {
      this.options = options != null ? options : {};
      this.api_url = __bind(this.api_url, this);

      this.sms = new Sms({
        'url': this.api_baseurl,
        'read_time': this.read_time
      });
      this.twitter = new Twitter({
        'url': this.api_baseurl,
        'read_time': this.read_time
      });
    }

    Api.prototype.api_url = function(url) {
      return this.api_baseurl + url;
    };

    Api.prototype.display = function() {
      var html, re, _i, _len, _ref;
      if (!this.messages) {
        return;
      }
      html = '';
      _ref = this.messages;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        re = _ref[_i];
        html += '\
<tr id="' + re.type + '-' + re.index + '" data-index="' + re.index + '" data-type="' + re.type + '" data-read="' + re.letto + '" data-timestamp="' + re.timestamp + '">\
  <td><i class="source-' + re.type + '"></i></td>\
  <td>' + re.mittente + '</td>\
  <td>' + re.messaggio + '</td>\
  <td>' + re.ora + '</td>\
</tr>';
      }
      if ($('table tbody').children().length === 0) {
        $('table tbody').html(html);
      } else {
        Tinycon.setBubble(this.messages.length);
        $('table tbody').prepend(html);
      }
      return this.messages = null;
    };

    Api.prototype.fetch_all = function() {
      var _this = this;
      this.sms.fetch();
      this.twitter.fetch();
      return $.doTimeout(1000, function() {
        _this._sort_results();
        return _this.display();
      });
    };

    Api.prototype.fetch_new = function() {
      var _this = this;
      this.sms.fetch_new();
      this.twitter.fetch_new();
      return $.doTimeout(1000, function() {
        _this._sort_results();
        return _this.display();
      });
    };

    Api.prototype._sort_results = function() {
      if (this.sms.response && this.twitter.response) {
        this.messages = this.sms.response.concat(this.twitter.response);
        this.messages.sort(function(a, b) {
          return b['timestamp'] - a['timestamp'];
        });
      } else if (this.sms.response && !this.twitter.response) {
        this.messages = this.sms.response;
      } else if (this.twitter.response && !this.sms.response) {
        this.messages = this.twitter.response;
      }
      this.sms.response = null;
      return this.twitter.response = null;
    };

    return Api;

  })();

}).call(this);
