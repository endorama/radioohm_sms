// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

// Place any jQuery/helper plugins in here.
/*
 * jQuery doTimeout: Like setTimeout, but better! - v1.0 - 3/3/2010
 * http://benalman.com/projects/jquery-dotimeout-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($){var a={},c="doTimeout",d=Array.prototype.slice;$[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};$.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?$.fn:$,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);
/*
 * Class:      countdown
 * Author:     Edoardo Tenani
 * Website:    http://about.me/edoardo.tenani
 * Version:    1.0.0
 * Date:       28/09/2012
 */
// (function(c){var b="countdown";function a(g,o){var e=g;var n=c(g);o=c.extend({},c.fn[b].defaults,o);function m(){n.addClass("countdown");n.data("reset",false);n.data("stop",false);n.data("to",null);f();return this}function i(){k();n.data("to",o.startNumber);d()}function f(){var r,q,p,s;p=o;n.addClass("running");return n.each(function(){_this=this;c.doTimeout("jQueryCountDown",p.duration,function(){if(c(_this).data("stop")==true){return false}s=c(_this).data("to");q=c(_this).data("reset");if(q||!s&&s!=p.endNumber){s=p.startNumber;c(_this).data("to",s);c(_this).data("reset",false)}if(s>p.endNumber){c(_this).text(s);c(_this).data("to",s-1)}else{c(_this).text("0");p.callBack(_this);k()}return true});return _this})}function d(){n.data("stop",false);n.removeClass("stopped");f()}function k(){n.data("stop",true);n.removeClass("running").addClass("stopped")}function h(p,q){if(q){o[p]=q}else{return o[p]}}function l(){n.each(function(){var q=this;var p=c(this);p.removeClass("countdown");p.removeData("reset");p.removeData("stop");p.removeData("to");p.removeData("plugin_"+b)})}function j(p){if(o[p]!==undefined){o[p].call(e)}}m();return{option:h,destroy:l,reset:i,start:d,stop:k}}c.fn[b]=function(f){if(typeof arguments[0]==="string"){var d=arguments[0];var e=Array.prototype.slice.call(arguments,1);var g;this.each(function(){if(c.data(this,"plugin_"+b)&&typeof c.data(this,"plugin_"+b)[d]==="function"){g=c.data(this,"plugin_"+b)[d].apply(this,e)}else{throw new Error("Method "+d+" does not exist on jQuery."+b)}});if(g!==undefined){return g}else{return this}}else{if(typeof f==="object"||!f){return this.each(function(){if(!c.data(this,"plugin_"+b)){c.data(this,"plugin_"+b,new a(this,f))}})}}};c.fn[b].defaults={duration:1000,startNumber:10,endNumber:0,callBack:function(){}}})(jQuery);
/*
 * Class:      countdown
 * Author:     Edoardo Tenani
 * Website:    http://about.me/edoardo.tenani
 * Version:    1.0.2
 * Date:       11-10-2012
 * Tested On:  jQuery 1.8.0
 * Require:    Bel Alman jQuery doTimeout plugin
 *             http://benalman.com/projects/jquery-dotimeout-plugin/
 * License:    MIT License
 *
 * Develop using the jQuery plugin boilerplate by Jonathan Nicol (@f6design)
 *   http://f6design.com/journal/2012/05/06/a-jquery-plugin-boilerplate/
 *
 * THE SOFTWARE IS PROVIDED ‘AS IS’, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
;(function($) {
  var pluginName = 'countdown';
 
  /**
   * Plugin object constructor.
   * Implements the Revealing Module Pattern.
   */
  function Plugin(element, options) {
    // References to DOM and jQuery versions of element.
    var el = element;
    var $el = $(element);

    // Extend default options with those supplied by user.
    options = $.extend({}, $.fn[pluginName].defaults, options);
 
    /**
     * Initialize the plugin instance.
     *
     * @access private
     */
    function init() {
      $el.addClass('countdown');

      $el.data('reset', false);
      $el.data('stop', false);
      $el.data('to', null);

      run();

      return this;
    }
 
    /**
     * Reset countdown current instance
     *
     * usage: $('#el').countdown('reset');
     *
     * @access public
     */
    function reset() { 
      $el.data('to', options.start);
    }

    /**
     * Run the countdown.
     *
     * @access private
     */
    function run() {
      var data, reset, settings, to;
      settings = options;

      $el.addClass('running');

      return $el.each(function() {
        _this = this;

        //loop
        $.doTimeout('jQueryCountDown', settings.duration, function() {

          if ($(_this).data('stop') == true)
            return false;

          to = $(_this).data('to');
          reset = $(_this).data('reset');

          if (reset || !to && to != settings.end) {
            to = settings.start;
            $(_this).data('to', to);
            $(_this).data('reset', false);
          }

          if (to > settings.end) {
            $(_this).text(to);
            $(_this).data('to', to - 1);
          }
          else {
            $(_this).text('0');
            // se la callback è true loopa, se è false fermalo. non serve lo start nella callback
            if (settings.callBack(_this) === false) {
              stop();
            }
          }

          // enable looping
          return true;
        });

        return _this;
      });
    }

    /**
     * Start the countdown after it has been stopped using stop().
     *
     * usage: $('#el').countdown('start');
     *
     * @access public
     */
    function start() {
      $el.data('stop', false);
      $el.removeClass('stopped');
      run();
    }

    /**
     * Stop the countdown.
     *
     * usage: $('#el').countdown('stop');
     *
     * @access public
     */
    function stop() {
      $el.data('stop', true);
      $el.removeClass('running').addClass('stopped');
    }
 
    /**
     * Get/set a plugin option.
     *
     * Get usage: $('#el').countdown('option', 'key');
     * Set usage: $('#el').countdown('option', 'key', value);
     *
     * @access public
     */
    function option(key, val) {
      if (val) {
        options[key] = val;
      } else {
        return options[key];
      }
    }
 
    /**
     * Destroy plugin.
     *
     * Usage: $('#el').countdown('destroy');
     *
     * @access public
     */
    function destroy() {
      // Iterate over each matching element.
      $el.each(function() {
        var el = this;
        var $el = $(this);
 
        $el.removeClass('countdown');

        $el.removeData('reset');
        $el.removeData('stop');
        $el.removeData('to'); 

        // Remove Plugin instance from the element.
        $el.removeData('plugin_' + pluginName);
      });
    }
 
    /**
     * Callback hooks.
     * Usage: In the defaults object specify a callback function:
     * hookName: function() {}
     * Then somewhere in the plugin trigger the callback:
     * hook('hookName');
     *
     * @access private
     */
    function hook(hookName) {
      if (options[hookName] !== undefined) {
        // Call the user defined function.
        // Scope is set to the jQuery element we are operating on.
        options[hookName].call(el);
      }
    }
 
    // Initialize the plugin instance.
    init();
 
    // Expose methods of Plugin we wish to be public.
    return {
      option: option,
      destroy: destroy,
      reset: reset,
      start: start,
      stop: stop
    };
  }
 
  /**
   * Plugin definition.
   */
  $.fn[pluginName] = function(options) {
    if (typeof arguments[0] === 'string') {
      var methodName = arguments[0];
      var args = Array.prototype.slice.call(arguments, 1);
      var returnVal;
      this.each(function() {
        if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
          returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);
        } else {
          throw new Error('Method ' +  methodName + ' does not exist on jQuery.' + pluginName);
        }
      });
      if (returnVal !== undefined){
        return returnVal;
      } else {
        return this;
      }
    } else if (typeof options === "object" || !options) {
      return this.each(function() {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    }
  };
 
  // Default plugin options.
  // Options can be overwritten when initializing plugin, by
  // passing an object literal, or after initialization:
  // $('#el').countdown('option', 'key', value);
  $.fn[pluginName].defaults = {
    duration: 1000,
    start: 10,
    end: 0,
    callBack: function() { }
  };
 
})(jQuery);
/*!
  Tinycon - A small library for manipulating the Favicon
  Tom Moor, http://tommoor.com
  Copyright (c) 2012 Tom Moor
  MIT Licensed
  @version 0.5
*/
(function(){var Tinycon={};var currentFavicon=null;var originalFavicon=null;var originalTitle=document.title;var faviconImage=null;var canvas=null;var options={};var defaults={width:7,height:9,font:'10px arial',colour:'#ffffff',background:'#F03D25',fallback:true,abbreviate:true};var ua=(function(){var agent=navigator.userAgent.toLowerCase();return function(browser){return agent.indexOf(browser)!==-1}}());var browser={ie:ua('msie'),chrome:ua('chrome'),webkit:ua('chrome')||ua('safari'),safari:ua('safari')&&!ua('chrome'),mozilla:ua('mozilla')&&!ua('chrome')&&!ua('safari')};var getFaviconTag=function(){var links=document.getElementsByTagName('link');for(var i=0,len=links.length;i<len;i++){if((links[i].getAttribute('rel')||'').match(/\bicon\b/)){return links[i]}}return false};var removeFaviconTag=function(){var links=document.getElementsByTagName('link');var head=document.getElementsByTagName('head')[0];for(var i=0,len=links.length;i<len;i++){var exists=(typeof(links[i])!=='undefined');if(exists&&(links[i].getAttribute('rel')||'').match(/\bicon\b/)){head.removeChild(links[i])}}};var getCurrentFavicon=function(){if(!originalFavicon||!currentFavicon){var tag=getFaviconTag();originalFavicon=currentFavicon=tag?tag.getAttribute('href'):'/favicon.ico'}return currentFavicon};var getCanvas=function(){if(!canvas){canvas=document.createElement("canvas");canvas.width=16;canvas.height=16}return canvas};var setFaviconTag=function(url){removeFaviconTag();var link=document.createElement('link');link.type='image/x-icon';link.rel='icon';link.href=url;document.getElementsByTagName('head')[0].appendChild(link)};var log=function(message){if(window.console)window.console.log(message)};var drawFavicon=function(label,colour){if(!getCanvas().getContext||browser.ie||browser.safari||options.fallback==='force'){return updateTitle(label)}var context=getCanvas().getContext("2d");var colour=colour||'#000000';var src=getCurrentFavicon();faviconImage=new Image();faviconImage.onload=function(){context.clearRect(0,0,16,16);context.drawImage(faviconImage,0,0,faviconImage.width,faviconImage.height,0,0,16,16);if((label+'').length>0)drawBubble(context,label,colour);refreshFavicon()};if(!src.match(/^data/)){faviconImage.crossOrigin='anonymous'}faviconImage.src=src};var updateTitle=function(label){if(options.fallback){if((label+'').length>0){document.title='('+label+') '+originalTitle}else{document.title=originalTitle}}};var drawBubble=function(context,label,colour){if(typeof label=='number'&&label>99&&options.abbreviate){label=abbreviateNumber(label)}var len=(label+'').length-1;var width=options.width+(6*len);var w=16-width;var h=16-options.height;context.font=(browser.webkit?'bold ':'')+options.font;context.fillStyle=options.background;context.strokeStyle=options.background;context.lineWidth=1;context.fillRect(w,h,width-1,options.height);context.beginPath();context.moveTo(w-0.5,h+1);context.lineTo(w-0.5,15);context.stroke();context.beginPath();context.moveTo(15.5,h+1);context.lineTo(15.5,15);context.stroke();context.beginPath();context.strokeStyle="rgba(0,0,0,0.3)";context.moveTo(w,16);context.lineTo(15,16);context.stroke();context.fillStyle=options.colour;context.textAlign="right";context.textBaseline="top";context.fillText(label,15,browser.mozilla?7:6)};var refreshFavicon=function(){if(!getCanvas().getContext)return;setFaviconTag(getCanvas().toDataURL())};var abbreviateNumber=function(label){var metricPrefixes=[['G',1000000000],['M',1000000],['k',1000]];for(var i=0;i<metricPrefixes.length;++i){if(label>=metricPrefixes[i][1]){label=round(label/metricPrefixes[i][1])+metricPrefixes[i][0];break}}return label};var round=function(value,precision){var number=new Number(value);return number.toFixed(precision)};Tinycon.setOptions=function(custom){options={};for(var key in defaults){options[key]=custom.hasOwnProperty(key)?custom[key]:defaults[key]}return this};Tinycon.setImage=function(url){currentFavicon=url;refreshFavicon();return this};Tinycon.setBubble=function(label,colour){label=label||'';drawFavicon(label,colour);return this};Tinycon.reset=function(){setFaviconTag(originalFavicon)};Tinycon.setOptions(defaults);window.Tinycon=Tinycon})();

