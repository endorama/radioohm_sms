/* Compiled by Sprockets! using therubyracer (V8) */
(function() {

  $(function() {
    if ($('section.flash').children().length !== 0) {
      return $('section.flash').fadeIn();
    }
  });

  window.Flash = (function() {

    function Flash() {}

    Flash.prototype.$flash_identifier = $('section.flash');

    Flash.prototype.add_flash = function(message, type) {
      var alert_button, css_class;
      if (type == null) {
        type = 'alert';
      }
      alert_button = '<button type="button" class="close" data-dismiss="alert">×</button>';
      if (type !== 'alert') {
        css_class = ' alert-' + type;
      } else {
        css_class = '';
      }
      return this.$flash_identifier.append('<div class="alert' + css_class + '"><button type="button" class="close" data-dismiss="alert">×</button>' + message + '</div>');
    };

    Flash.prototype.display = function() {};

    Flash.prototype.hide = function() {
      this.$flash_identifier.fadeOut();
      return this.$flash_identifier.html('');
    };

    Flash.prototype.add_alert = function(message) {
      return this.add_flash(message);
    };

    Flash.prototype.add_error = function(message) {
      return this.add_flash(message, 'error');
    };

    Flash.prototype.add_info = function(message) {
      return this.add_flash(message, 'info');
    };

    Flash.prototype.add_success = function(message) {
      return this.add_flash(message, 'success');
    };

    Flash.prototype.display_alert = function(message) {
      this.add_alert(message);
      return this.display();
    };

    Flash.prototype.display_error = function(message) {
      this.add_error(message);
      return this.display();
    };

    Flash.prototype.display_info = function(message) {
      this.add_info(message);
      return this.display();
    };

    Flash.prototype.display_success = function(message) {
      this.add_success(message);
      return this.display();
    };

    return Flash;

  })();

}).call(this);
