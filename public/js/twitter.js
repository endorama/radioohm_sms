/* Compiled by Sprockets! using therubyracer (V8) */
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Twitter = (function() {

    function Twitter(options) {
      this.options = options != null ? options : {};
      this.read = __bind(this.read, this);

      this.fetch_new = __bind(this.fetch_new, this);

      this.fetch = __bind(this.fetch, this);

      this.url = this.options.url + 'twitter';
      this.read_time = this.options.read_time;
    }

    Twitter.prototype.fetch = function(id) {
      var url,
        _this = this;
      if (id) {
        url = this.url + ("/" + id);
      } else {
        url = this.url;
      }
      return $.ajax({
        url: url,
        type: 'GET',
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          _this.response = _this._add_type(response);
          return Flash.prototype.display_info('Fetched twitter messages.');
        }
      });
    };

    Twitter.prototype.fetch_new = function() {
      var _this = this;
      return $.ajax({
        url: this.url + "/new",
        type: 'GET',
        data: {
          'index': $($('[data-type="twitter"]')[0]).attr('data-index')
        },
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          if (response !== 'null') {
            _this.response = _this._add_type(response);
            Flash.prototype.display_info('New messages.');
          } else {
            Flash.prototype.hide();
            Flash.prototype.display_info('There are no new messages.');
          }
          return $.doTimeout(_this.read_time, function() {
            return Flash.prototype.hide();
          });
        }
      });
    };

    Twitter.prototype.read = function(id) {
      return $.ajax({
        url: this.url + ("/" + id),
        type: 'PUT',
        data: {
          letto: 1
        },
        error: function(response) {
          return alert(response.responseText);
        },
        success: function(response) {
          response = response[0];
          return $('#twitter-' + id).addClass('read').data('read', response.letto);
        }
      });
    };

    Twitter.prototype._add_type = function(response) {
      var re, _i, _len;
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        re = response[_i];
        re.type = 'twitter';
      }
      return response;
    };

    return Twitter;

  })();

}).call(this);
