<?php
  use MtHaml\Autoloader;
  use MtHaml\Environment;

  require_once __DIR__ . '/../compiler/lib/MtHaml/lib/MtHaml/Autoloader.php';
  MtHaml\Autoloader::register();
  ?>
  <!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>RadioOhm Live Messages</title>
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/screen.css" />
    <script src="js/vendor/modernizr-2.6.1.min.js"></script>
  </head>
  <body>
    <div id="container" class="<?php echo '<?'; ?>php echo $asd ?>">
      <?php $asd = "test"; ?>
      <?php echo $asd; ?>
      <header id="header">
        <div class="navbar navbar-fixed-top navbar-inverse">
          <div class="navbar-inner">
            <a <?php echo MtHaml\Runtime::renderAttributes(array(array('class', 'btn'), array('class', 'btn-navbar'), array(('data-toggle'), 'collapse'), array(('data-target'), '.nav-collapse')), 'html5', 'UTF-8'); ?>>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="brand" href="#">
              RadioOhm.it
            </a>
            <div class="nav-collapse">
              <ul class="nav">
                <li>
                  <a <?php echo MtHaml\Runtime::renderAttributes(array(array('href', ('#')), array(('data-action'), ('to-top')), array(('data-toggle'), 'tooltip'), array('title', 'Torna a inizio pagina')), 'html5', 'UTF-8'); ?>>
                    <i class="icon-circle-arrow-up icon-white"></i>
                  </a>
                </li>
                <li>
                  <a <?php echo MtHaml\Runtime::renderAttributes(array(array('href', ('#')), array(('data-action'), ('load-new-sms')), array(('data-toggle'), 'tooltip'), array('title', 'Verifica la presenza di nuovi sms')), 'html5', 'UTF-8'); ?>>
                    <i class="icon-refresh icon-white"></i>
                  </a>
                </li>
                <li>
                  <a <?php echo MtHaml\Runtime::renderAttributes(array(array('href', ('#')), array(('data-action'), ('read-all')), array(('data-toggle'), 'tooltip'), array('title', 'Segna come letti tutti i messaggi')), 'html5', 'UTF-8'); ?>>
                    <i class="icon-envelope icon-white"></i>
                  </a>
                </li>
                <li class="dropdown">
                  <a <?php echo MtHaml\Runtime::renderAttributes(array(array('class', 'dropdown-toggle'), array(('data-toggle'), 'dropdown'), array('href', '#'), array(('data-action'), ('automatic-sms-refresh'))), 'html5', 'UTF-8'); ?>>
                    Aggiornameto automatico
                  </a>
                  <ul <?php echo MtHaml\Runtime::renderAttributes(array(array('class', 'dropdown-menu'), array('role', 'menu'), array(('aria-labelledby'), 'dLabel')), 'html5', 'UTF-8'); ?>>
                    <li>
                      <a <?php echo MtHaml\Runtime::renderAttributes(array(array('href', ('#')), array(('data-action'), ('enable-automatic-sms-refresh')), array(('data-toggle'), 'popover'), array('title', ''), array(('data-content'), 'Se abilitato, ogni 30 secondi il programma caricherà i nuovi sms, se ve ne sono')), 'html5', 'UTF-8'); ?>>
                        Abilitato
                        <i class="icon-ok"></i>
                      </a>
                    </li>
                    <li>
                      <a <?php echo MtHaml\Runtime::renderAttributes(array(array('href', ('#')), array(('data-action'), ('disable-automatic-sms-refresh')), array(('data-toggle'), 'popover'), array('title', ''), array(('data-content'), 'Il programma non cercherà nuovi sms. Puoi aggiornare manualmente con il pulsante nella barra superiore!')), 'html5', 'UTF-8'); ?>>
                        Disabilitato
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <p class="navbar-text countdown-container">
              Prossimo aggiornamento automatico
              <span id="countdown">mai</span>
            </p>
          </div>
        </div>
      </header>
      <div <?php echo MtHaml\Runtime::renderAttributes(array(array('id', 'main'), array('role', ('main'))), 'html5', 'UTF-8'); ?>>
        <section class="flash"></section>
        <section class="help">
          <p>
            Il numero di telefono di RadioOhm &egrave;
            <b>348 545 4218</b>
          </p>
          <p>
            L'hashtag da utilizzare su twitter &egrave;
            <b>#radioohmlive</b>
          </p>
        </section>
        <table class="table table-striped table-condensed table-bordered table-hover">
          <thead>
            <th>Origine</th>
            <th>Mittente</th>
            <th>Messaggio</th>
            <th>Ora</th>
          </thead>
          <tbody></tbody>
          <tfoot>
            <th>Origine</th>
            <th>Mittente</th>
            <th>Messaggio</th>
            <th>Ora</th>
          </tfoot>
        </table>
      </div>
      <footer id="footer"></footer>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>
      window.jQuery || document.write("<script src='js/vendor/jquery-1.8.0.min.js'>\x3C/script>")
    </script>
    <script src="js/vendor/bootstrap.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/application.js"></script>
    <!--[if lt IE 7]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>
      window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})});
    </script>
    <![endif]-->
  </body>
</html>

  <?php return true; ?>