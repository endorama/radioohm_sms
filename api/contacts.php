<?php

require_once '../config.php';
require_once 'response.class.php';
require_once 'models/contact.class.php';
require_once 'baseApi.class.php';

class Contacts extends BaseApi {

  protected $association = 'contactClass';

  ##############################################################################
  ##
  ## Public API
  ##

  /**
   * Get all contacts.
   * 
   * @url GET /
   */
  function index() {
    $result = $this->_get();

    return empty($result) ? Response::respond_with_null() : $result;
  }

  /**
   * Get a single contanct.
   * 
   * @url GET /:id
   */
  function get($id = NULL) {
    $result = $this->_get($id);

    return empty($result) ? Response::respond_with_null() : $result;
  }
    
  /**
   * Create a new Contact.
   * 
   * @url POST /
   */
  function post($request_data = NULL) {
    $this->_validate_post_data($request_data);
    
    $result = $this->_post($request_data);

    return empty($result) ? Response::respond_with_false() : $result;
  }

  /**
   * Update a single contact.
   * 
   * @url PUT /:id
   */
  function put($id = NULL, $request_data = NULL) {
    $this->_validate_put_data($id, $request_data);
    $result = $this->_put($id, $request_data);

    return empty($result) ? Response::respond_with_null() : $result;
  }

  ##############################################################################
  ##
  ## Private | Protected
  ##

  private function _get($id = NULL) {
    if ($id)
      return $this->_query('get', 'SELECT * FROM livesms_contacts WHERE `id` = :id', array(':id' => $id));
    else
      return $this->_query('get', 'SELECT * FROM livesms_contacts');
  }

  private function _post($request_data) {
    $result = $this->_query('insert', 'INSERT INTO livesms_contacts
        (name, surname, phone_number)
        VALUES (:name, :surname, :phone_number)', 
      array(
        'name' => $request_data['name'],
        'surname' => $request_data['surname'],
        'phone_number' => $request_data['phone_number']
      ));

    return ($result) ? $this->get_last_insert_id() : FALSE;
  }

  private function _put($id, $request_data) { 
    $result = $this->_query('update', 'UPDATE livesms_contacts
        SET name = :name, surname = :surname, phone_number = :phone_number
        WHERE `id` = :id', 
      array(
        'name' => $request_data['name'],
        'surname' => $request_data['surname'],
        'phone_number' => $request_data['phone_number'],
        'id' => $id
      ));

    return ($result) ? $this->_get($id) : FALSE;
  }

  private function _validate_post_data($request_data) {
    if (!$request_data ||
        !$request_data['name'] ||
        !$request_data['surname'] ||
        !$request_data['phone_number'])
      Response::respond_with_412('Arguments passed where not a valid array of arguments');
  }

  private function _validate_put_data($id, $request_data) {
    if (!is_numeric($id))    Response::respond_with_412("argument 'id' is not a valid number");
    $this->_validate_post_data($request_data);
  }

}
