<?php

require_once 'restler/restler.php';
require_once 'restler/compat.php';

require_once 'sms.php';
require_once 'contacts.php';
require_once 'twitter.php';

// spl_autoload_register('spl_autoload');

$r = new Restler();

// $r = new Restler(TRUE); // sending true enables production mode
// $r->refreshCache();

// $r->setSupportedFormats();

$r->addAPIClass('Sms');
$r->addAPIClass('Contacts');
$r->addAPIClass('Twitter');

$r->handle();

var_dump($r);
