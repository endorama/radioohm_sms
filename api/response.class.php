<?php

class Response {
  
  /**
   * Respond to the request with NULL value.
   */
  public static function respond_with_null($format = 'json') {
    switch ($format) {
      case 'json':
        $resp = json_encode(NULL);
        break;
      
      default:
        die('Response::respond_with_null() a format must be specified');
        break;
    }
    
    return $resp;
  }

  public static function respond_with_false($format = 'json') {
    switch ($format) {
      case 'json':
        $resp = json_encode(FALSE);
        break;
      
      default:
        die('Response::respond_with_false() a format must be specified');
        break;
    }
    
    return $resp;
  }

  
  /**
   * Respond with a 400 HTTP header.
   */
  public static function respond_with_400() {
    throw new RestException(400);
  }

  /**
   * Respond with a 412 HTTP header.
   */
  public static function respond_with_412($msg) {
    throw new RestException(412, $msg);
  }
  
}
