<?php

require_once '../config.php';
// require_once 'response.class.php';
// require_once 'models/sms.class.php';

class BaseApi extends StdClass {

  /**
   * A valid connection to the database.
   * PDO library is used.
   * 
   * @var resource
   */
  protected $conn;

  /**
   * Associate the class instance to a PHP class, for simple object mapping with
   *  PDO.
   * 
   * @var string
   */
  protected $association = 'StdClass';

  /**
   * If TRUE, enable debug ouput.
   * 
   * @var bool
   */
  protected $debug = FALSE;

  function __construct() {
    global $config;

    try {
      $this->conn = new PDO($config['db']['type'] . ':host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'], $config['db']['username'], $config['db']['password']);
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  function __destruct() {
    $this->conn = NULL;  
  }

  ##############################################################################
  ##
  ## Public API
  ##
    

  /**
   * API Method not implemented.
   * 
   * For a best CRUD, all not implementd method should be here, returning a 501
   * not implemented HTTP status code.
   */
  function get($id = NULL) {
    throw new RestException(405);
  }
  function post($request_data = NULL) {
    throw new RestException(405);
  }
  function put($id = NULL, $request_data = NULL) {
    throw new RestException(405);
  }
  function delete($id = NULL) {
    throw new RestException(405);
  }

  ##############################################################################
  ##
  ## Private | Protected
  ##

  protected function get_last_insert_id() {
    return $this->conn->lastInsertId();
  }

  /**
   * Performs a query on the database.
   * 
   * @param string $query
   *   the SQL statement, with placeholders
   * @param array $args
   *   placeholders values according to the PDO library specifications
   * 
   * @return array|NULL
   *   an array of SmsClass object or NULL
   */
  protected function _query($type = 'get', $query, $args = NULL) {
    try {
      $stmt = $this->conn->prepare($query);
      $result = $stmt->execute($args);

      if ($this->debug)
        echo "<pre>", var_dump($stmt), "</pre>";

      if ($type == 'get') {
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->association);
        $result = $stmt->fetchAll();

        foreach ($result as $value)
          $value->process_after_fetch();

        if ($this->debug)
          echo "<pre>", var_dump($result), "</pre>";

      }
      
      return $result;
    }
    catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }

    return NULL;
  }
}
