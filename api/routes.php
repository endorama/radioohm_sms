<?php $o=array();



############### GET ###############

$o['GET']=array();

#==== GET sms/new

$o['GET']['sms/new']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'fetch_new',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Get all messages newer than $timestamp.',
	    'url' => 'GET /new',
	  ),
	  'method_flag' => 0,
	);

#==== GET sms/:id

$o['GET']['sms/:id']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'get',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Show a specific sms.',
	    'url' => 'GET /:id',
	  ),
	  'method_flag' => 0,
	);

#==== GET sms

$o['GET']['sms']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'index',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Return last 50 sms on database.',
	    'url' => 'GET /',
	  ),
	  'method_flag' => 0,
	);

#==== GET sms/_last_insert_id

$o['GET']['sms/_last_insert_id']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'get_last_insert_id',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 3,
	);

#==== GET contacts

$o['GET']['contacts']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'index',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Get all contacts.',
	    'url' => 'GET /',
	  ),
	  'method_flag' => 0,
	);

#==== GET contacts/:id

$o['GET']['contacts/:id']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'get',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Get a single contanct.',
	    'url' => 'GET /:id',
	  ),
	  'method_flag' => 0,
	);

#==== GET contacts/_last_insert_id

$o['GET']['contacts/_last_insert_id']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'get_last_insert_id',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 3,
	);

#==== GET twitter/new

$o['GET']['twitter/new']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'fetch_new',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Get all messages newer than $timestamp.',
	    'url' => 'GET /new',
	  ),
	  'method_flag' => 0,
	);

#==== GET twitter/:id

$o['GET']['twitter/:id']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'get',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Show a specific sms.',
	    'url' => 'GET /:id',
	  ),
	  'method_flag' => 0,
	);

#==== GET twitter

$o['GET']['twitter']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'index',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Return last 50 sms on database.',
	    'url' => 'GET /',
	  ),
	  'method_flag' => 0,
	);

#==== GET twitter/_last_insert_id

$o['GET']['twitter/_last_insert_id']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'get_last_insert_id',
	  'arguments' => 
	  array (
	  ),
	  'defaults' => 
	  array (
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 3,
	);


############### PUT ###############

$o['PUT']=array();

#==== PUT sms/:id

$o['PUT']['sms/:id']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'put',
	  'arguments' => 
	  array (
	    'id' => 0,
	    'request_data' => 1,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	    1 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Update a single sms',
	    'url' => 'PUT /:id',
	  ),
	  'method_flag' => 0,
	);

#==== PUT contacts/:id

$o['PUT']['contacts/:id']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'put',
	  'arguments' => 
	  array (
	    'id' => 0,
	    'request_data' => 1,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	    1 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Update a single contact.',
	    'url' => 'PUT /:id',
	  ),
	  'method_flag' => 0,
	);

#==== PUT twitter/:id

$o['PUT']['twitter/:id']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'put',
	  'arguments' => 
	  array (
	    'id' => 0,
	    'request_data' => 1,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	    1 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Update a single sms',
	    'url' => 'PUT /:id',
	  ),
	  'method_flag' => 0,
	);


############### POST ###############

$o['POST']=array();

#==== POST sms

$o['POST']['sms']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'post',
	  'arguments' => 
	  array (
	    'request_data' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== POST contacts

$o['POST']['contacts']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'post',
	  'arguments' => 
	  array (
	    'request_data' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	    'description' => 'Create a new Contact.',
	    'url' => 'POST /',
	  ),
	  'method_flag' => 0,
	);

#==== POST twitter

$o['POST']['twitter']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'post',
	  'arguments' => 
	  array (
	    'request_data' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);


############### DELETE ###############

$o['DELETE']=array();

#==== DELETE sms

$o['DELETE']['sms']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'delete',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== DELETE sms/:id

$o['DELETE']['sms/:id']=array (
	  'class_name' => 'Sms',
	  'method_name' => 'delete',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== DELETE contacts

$o['DELETE']['contacts']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'delete',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== DELETE contacts/:id

$o['DELETE']['contacts/:id']=array (
	  'class_name' => 'Contacts',
	  'method_name' => 'delete',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== DELETE twitter

$o['DELETE']['twitter']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'delete',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== DELETE twitter/:id

$o['DELETE']['twitter/:id']=array (
	  'class_name' => 'Twitter',
	  'method_name' => 'delete',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);
return $o;