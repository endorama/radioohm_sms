<?php

require_once '../config.php';
require_once 'response.class.php';
require_once 'models/twitter.class.php';
require_once 'baseApi.class.php';

class Twitter extends BaseApi {

  protected $association = 'TwitterClass';

  ##############################################################################
  ##
  ## Public API
  ##
    
  /**
   * Get all messages newer than $timestamp.
   * 
   * @url GET /new
   */
  function fetch_new($index) {
    $result = $this->_get_new_messages($index);
    
    return (empty($result)) ? Response::respond_with_null() : $result;
  }

  /**
   * Show a specific sms.
   * 
   * @url GET /:id
   */
  function get($id = NULL) {
    $this->_validate_get($id);

    $result = $this->_get($id);

    // var_dump($this->restler);

    return empty($result) ? Response::respond_with_null() : $result;
  }

  /**
   * Return last 50 sms on database.
   * 
   * @url GET /
   */
  function index() {
    $result = $this->_get();

    return empty($result) ? Response::respond_with_null() : $result;
  }

  /**
   * Update a single sms
   * 
   * @url PUT /:id
   */
  function put($id = NULL, $request_data = NULL) {
    if (!$id)
      throw new RestException(400);

    $this->_validate_update($request_data);
      
    return $this->_update($id, $request_data);
  }

  
  ##############################################################################
  ##
  ## Private | Protected
  ##

  /**
   * Get sms from database.
   * 
   * @param (optional) $index
   *   if specified, query the database for the sms with specified index
   *
   * @return array|NULL
   *   an array of SmsClass object or NULL
   */
  private function _get($index = NULL) {
    if ($index)
      return $this->_query('get', 'SELECT * FROM twitter_messages WHERE `index` = :index', array(':index' => $index));
    else
      return $this->_query('get', 'SELECT * FROM twitter_messages ORDER BY `ora` DESC');
  }

  private function _get_new_messages($index) {

    // return $this->_query('get', 'SELECT * FROM twitter_messages WHERE `letto` = 0 ORDER BY `ora` DESC');
    // return $this->_query('get', 'SELECT * FROM `twitter_messages` WHERE `index` > "' . $index . '" ORDER BY `ora` DESC');
    return $this->_query('get', 'SELECT * FROM `twitter_messages` WHERE `index` > "' . $index . '" ORDER BY `index` DESC');

  }

  private function _update($id, $data) {
    $result = $this->_query('update', 'UPDATE twitter_messages
        SET letto = :letto
      WHERE `index` = :index', 
      array(
        'index' => $id,
        'letto' => $data['letto']
      ));

    return ($result) ? $this->_get($id) : FALSE;
  }

  /**
   * Validate request data for GET HTTP request.
   * 
   * Should be used to validate user submitted data before performing any sql 
   * query.
   * 
   * @param int $id
   *   The data passed with the HTTP request.
   * 
   * @return void
   */
  function _validate_get($id) {
    if (!is_numeric($id))    Response::respond_with_412("argument 'id' is not a valid number");
  }

  /**
   * Validate request data for UPDATE HTTP request.
   * 
   * Should be used to validate user submitted data before performing any sql 
   * query.
   * 
   * @param array $data
   *   The data passed with the HTTP request.
   * 
   * @return void
   */
  private function _validate_update($data){
    if ($this->debug)
      echo "<pre>", var_dump($data), "</pre>";

    if (!isset($data['letto']))       Response::respond_with_400();
    if (!is_numeric($data['letto']))  Response::respond_with_412("argument 'letto' is not a valid number");
    if ($data['letto'] != '0' &&
        $data['letto'] != '1')        Response::respond_with_412("argument 'letto' has not a valid. Must be 0 or 1 only.");

  }

}
