<?php

class TwitterClass extends StdClass {
  /**
   * bigint(20)
   */
  private $twit_id;
  /**
   * text
   */
  public $mittente;
  /**
   * text
   */
  public $messaggio;
  /**
   * text
   */
  public $ora;
  /**
   * tinyint(4)
   */
  public $letto;
  /**
   * int
   */
  public $index;

  /**
   * the timestamp calculated on $this->ora.
   */
  public $timestamp;

  
  /**
   * Perform some operation to fill all datas.
   */
  public function process_after_fetch() {
    $this->calculate_timestamp();
  }

  ##############################################################################

  /**
   * Calculate timestamp from $this->ora value.
   */
  private function calculate_timestamp() {
    $ora = str_replace("/", '-', $this->ora);

    $this->timestamp = strtotime($ora);
  }
}
