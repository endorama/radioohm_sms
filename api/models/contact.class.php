<?php

class ContactClass extends StdClass {
  /**
   * int(11)
   */
  private $id;
  /**
   * varchar(50)
   */
  public $name;
  /**
   * varchar(50)
   */
  public $surname;
  /**
   * varchar(50)
   */
  public $phone_number;
  
  /**
   * Perform some operation to fill all datas.
   */
  public function process_after_fetch() {
    $this->calculate_display_name();
  }

  ##############################################################################

  /**
   * Calculate timestamp from $this->ora value.
   */
  private function calculate_display_name() {
    $this->display_name = $this->name . " " . $this->surname;
  }
}
