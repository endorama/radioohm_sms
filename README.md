I requisiti dovrebbero essere:
  1. interfaccia divisa in 2 parti stile client di posta (sopra elenco messaggi, sotto lettura completa del messaggio selezionato)
  2. auto refresh della lista dei messaggi senza ricaricare la pagina ( ogni 30/60 secondi )

Nel db inoltre c'è un campo booleano si chiama letto sarebbe da utilizzare per segnare in grassetto i messaggi non letti.


# Documentazione

[PHP PDO](http://net.tutsplus.com/tutorials/php/php-database-access-are-you-doing-it-correctly/)
[Twitter Bootstrap](http://twitter.github.com/bootstrap/)
[CoffeeScript CookBook](http://coffeescriptcookbook.com/)
[The Little Book of CoffeeScript](http://arcturo.github.com/library/coffeescript/index.html)
[jQuery DoTimeout](http://benalman.com/projects/jquery-dotimeout-plugin/)
[jQuery JSONP](https://github.com/jaubourg/jquery-jsonp)
[jQuery Twitter API](http://code.google.com/p/jquery-twitter-api/)

[Twitter Dev API](https://dev.twitter.com/docs/using-search)
[Twitter hastag search](http://search.twitter.com/search.json?q=%23direttaradioohm)

# Struttura

Obtained by `tree -F -L 1 --dirsfirst`

    radioohm_sms                        Root Folder
    ├── api/                            Contains API definition
    ├── lib/                            Library used in compilation
    ├── public/                         Contains the web frontend
    ├── theme/                          Source theme files
    ├── cliColors.class.php             CLI color library for PHP
    ├── compiler.php                    Compile source theme in PHP, CSS and JS
    ├── config.php                      Application configurations
    ├── haml.php                        HAML compiler
    ├── process.class.php               Handle process execution
    ├── README.md                       This file :)
    ├── sass.php                        SASS compiler
    └── sprockets.php                   CoffeeScript compiler
