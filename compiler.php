<?php
require 'compiler/cliColors.class.php';
require 'compiler/haml.php';
require 'compiler/sprockets.php';
require 'compiler/sass.php';

$script_name = array_shift($argv);

$options = array(
  'haml' => FALSE,
  'coffee' => FALSE,
  'sass' => FALSE
);

if (count($argv) == 0)
  $options = array(
    'haml' => TRUE,
    'coffee' => TRUE,
    'sass' => TRUE
    );
else
  foreach ($argv as $arg)
    $options[$arg] = TRUE;


if ($options['haml']) {
  CliColors::out("Compiling HAML...         ");

  list($retcode, $stderr) = compile_haml();
  if ($retcode == 0)
    CliColors::line("[ DONE ]", 'green');
  else
    CliColors::line($stderr, 'red');
}

if ($options['coffee']) {
  CliColors::out("Compiling CoffeeScript...");

  list($retcode, $stderr) = compile_coffescript();
  if ($retcode == 0)
    CliColors::line(" [ DONE ]", 'green');
  else
    CliColors::line($stderr, 'red');
}

if ($options['sass']) {
  CliColors::out("Compiling SASS...         ");

  list($retcode, $stderr) = compile_sass();
  if ($retcode == 0)
    CliColors::line("[ DONE ]", 'green');
  else
    CliColors::line($stderr, 'red');
}

echo 'Compilation completed';
