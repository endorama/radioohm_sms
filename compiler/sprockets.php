<?php

require_once 'process.class.php';

function compile_coffescript() {
  $files = glob('theme/coffee/*.coffee');
  $responses = array();

  foreach ($files as $template) {
    $resp = sprockets_compile($template);

    $file = 'public/js/' . basename($template, '.coffee') . '.js';
    
    file_put_contents($file, $resp[0]);

    $responses[] = $resp;
  }

  $stderr = '';
  $exitcode = 0;
  array_map(function($i) use (&$exitcode, &$stderr) {
    $exitcode = max($exitcode, $i[2]['exitcode']);
    $stderr .= $i[1];
  }, $responses);

  return array($exitcode, $stderr);
}

function sprockets_compile($template) {
  $cmd = '/opt/wordless/ruby compiler/lib/rb/sprockets_config.rb compile ' . $template . ' \
    --paths \
      theme/coffee \
      public/js';
/*       \
    --compress \
    --munge';
*/
  $prc = new Process($cmd);

  return $prc->run();
}
 
