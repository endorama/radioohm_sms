<?php

use MtHaml\Autoloader;
use MtHaml\Environment;

require_once __DIR__ . '/lib/MtHaml/lib/MtHaml/Autoloader.php';
MtHaml\Autoloader::register();

require_once 'process.class.php';

$parser_options =  array(
  'format' => 'html5',
  'enable_escaper' => FALSE,
  'escape_html' => TRUE,
  'escape_attrs' => TRUE,
  'autoclose' => array(
    'meta',
    'img',
    'link',
    'br',
    'hr',
    'input',
    'area',
    'param',
    'col',
    'base'
  ),
  'charset' => 'UTF-8');

$haml = new MtHaml\Environment('php',  $parser_options);

function compile_haml() {
  $files = glob('theme/haml/*.haml');
  $responses = array();

  foreach ($files as $template) {
    $haml_compiled = mthaml_compile($template);
    $file = 'public/' . basename($template, '.haml') . '.php';

    file_put_contents($file, $haml_compiled);

    $prc = new Process('php -l ' . $file);
    $responses[] = $prc->run();
  }

  $stderr = '';
  $exitcode = 0;
  array_map(function($i) use (&$exitcode, &$stderr) {
    $exitcode = max($exitcode, $i[2]['exitcode']);
    $stderr .= $i[1];
  }, $responses);

  return array($exitcode, $stderr);
}

function mthaml_compile($template) {
  global $haml;
  $compiled = $haml->compileString(file_get_contents($template), "filename");

  $compiled = "<?php
  use MtHaml\Autoloader;
  use MtHaml\Environment;

  require_once __DIR__ . '/../compiler/lib/MtHaml/lib/MtHaml/Autoloader.php';
  MtHaml\Autoloader::register();
  ?>
  " . $compiled . "
  <?php return true; ?>";
  
  return $compiled;
}
