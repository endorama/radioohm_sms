<?php
 
class CliColors {
  public static $fg_colors = array(
    'black' => '30',
    'blue' => '34',
    'green' => '32',
    'cyan' => '36',
    'red' => '31',
    'purple' => '35',
    'brown' => '33',
    'light_gray' => '37',
  );
  public static $bg_colors = array(
    'black' => '40',
    'red' => '41',
    'green' => '42',
    'yellow' => '43',
    'blue' => '44',
    'magenta' => '45',
    'cyan' => '46',
    'light_gray' => '47',
  );
  public static $bold = FALSE;

  // Returns colored string
  public static function highlight($string, $fg = NULL, $bold = NULL, $bg = NULL) {
    $bold = $bold ? $bold : self::$bold;

    if (( $fg && !isset(self::$fg_colors[$fg]) ) 
        ||
        ( $bg && !isset(self::$bg_colors[$bg]) ))
      return false;

    $fg = $fg ? ( "\033[" . ($bold ? '1;' : '0;') . self::$fg_colors[$fg] ) : NULL;
    $bg = $bg ? ( "\033[" .  self::$bg_colors[$bg] ) : NULL;

    // Add string and end coloring ( if $fg or $bg exists )
    return  ($fg || $bg) ? ($fg . $bg . "m" . $string . "\033[0m") : $string;
  }

  public static function line($string, $fg = NULL, $bold = NULL, $bg = NULL)  {
    echo self::out($string, $fg, $bold, $bg) . "\n";
  }

  public static function out($string, $fg = NULL, $bold = NULL, $bg = NULL) {
    echo self::highlight($string, $fg, $bold, $bg);
  }

}
 
?>
