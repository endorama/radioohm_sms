http_path = "/radioohm_sms"
css_dir = "public/css"
sass_dir = "theme/sass"
images_dir = "public/img"
javascripts_dir = "public/js"
relative_assets = false
line_comments = true
assets_cache = true
preferred_syntax = :sass
asset_cache_buster :none
