<?php

class Process {
  
  var $cmd = NULL;

  public function __construct($cmd) {
    $this->cmd = $cmd;
  }
  
  public function run() {
    if (!$this->cmd)
      return false;

    $process = proc_open(
      $this->cmd, # command
      array(array('pipe', 'r'), array('pipe', 'w'), array('pipe', 'w')), # descriptors
      $pipes,
      getcwd(), # cwd
      NULL, # environment
      array('suppress_errors' => true, 'binary_pipes' => true, 'bypass_shell' => false) # options
    );

    # if isn't a resource something went wrong
    if (!is_resource($process))
      throw new Exception('Unable to launch a new process.');

    # write stdin to pipe 0, launch the process
    fwrite($pipes[0], '');
    $status = proc_get_status($process);
    $stdout = '';
    $stderr = '';

    # get stdout and stderr, gather information about working process
    while($status['running']) {
      $stdout .= stream_get_contents($pipes[1]);
      $stderr .= stream_get_contents($pipes[2]);
      $status = proc_get_status($process);
    }

    # closing pipes
    fclose($pipes[0]);
    fclose($pipes[1]);
    fclose($pipes[2]);

    return array($stdout, $stderr, $status);
  }
}
