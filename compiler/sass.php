<?php

require_once 'process.class.php';

function compile_sass() {
  $files = glob('theme/sass/*.sass');
  $responses = array();

  foreach ($files as $template) {
    $responses[] = compass_compile($template);
    
    $file = 'public/css/' . basename($template, '.sass') . '.css';
  }

  $stderr = '';
  $exitcode = 0;
  array_map(function($i) use (&$exitcode, &$stderr) {
    $exitcode = max($exitcode, $i[2]['exitcode']);
    $stderr .= $i[1];
  }, $responses);

  return array($exitcode, $stderr);
}

function compass_compile($template) {
  $cmd = '/opt/wordless/compass compile -c compiler/lib/rb/compass_config.rb --boring ' . $template;
    
  $prc = new Process($cmd);
  return $prc->run();
}
